package com.ebanking.ebanking.config;

import com.ebanking.ebanking.Token.JwtConfigurer;
import com.ebanking.ebanking.Token.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource()
    {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("*"));
        configuration.setAllowedMethods(Arrays.asList("GET","POST"));
        configuration.setAllowedHeaders(Arrays.asList("Authorization", "Cache-Control", "Content-Type", "Access-Control-Allow-Origin", "access-control-allow-headers"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //@formatter:off
        http
                .cors().and()
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/auth/login").permitAll()
                .antMatchers("/users").permitAll()
                .antMatchers("/user/**").permitAll()
                .antMatchers("/user/**/**").permitAll()

                .antMatchers("/accounts").permitAll()
                .antMatchers("/account/**").permitAll()
                .antMatchers("/account/**/**").permitAll()

                .antMatchers("/transactions").permitAll()
                .antMatchers("/transaction/**").permitAll()
                .antMatchers("/transaction/**/**").permitAll()
                .antMatchers("/transaction/**/**/**/**").permitAll()
                /*.antMatchers(HttpMethod.GET, "/users").hasRole("USER")
                .antMatchers(HttpMethod.POST, "/user").permitAll()
                .antMatchers(HttpMethod.DELETE, "/user/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/user/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.GET, "/user/**").hasRole("USER")

                .antMatchers(HttpMethod.GET, "/accounts").hasRole("USER")
                .antMatchers(HttpMethod.POST, "/accounts").hasRole("USER")
                .antMatchers(HttpMethod.GET, "/account/**").hasRole("USER")
                .antMatchers(HttpMethod.PUT, "/account/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/account/**").hasRole("ADMIN")

                .antMatchers(HttpMethod.GET, "/transactions").hasRole("USER")
                .antMatchers(HttpMethod.POST, "/transaction").hasRole("USER")
                .antMatchers(HttpMethod.GET, "/transaction/**").hasRole("USER")
                .antMatchers(HttpMethod.PUT, "/transaction/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/transaction/**").hasRole("ADMIN")*/

                .anyRequest().permitAll()
                .and()
                .apply(new JwtConfigurer(jwtTokenProvider));

        //@formatter:on
    }
}
package com.ebanking.ebanking.Services;

import com.ebanking.ebanking.Exceptions.DuplicateUserNameException;
import com.ebanking.ebanking.Exceptions.UserNotFoundException;
import com.ebanking.ebanking.Models.BankAccount;
import com.ebanking.ebanking.Models.User;
import com.ebanking.ebanking.Repositories.BankAccountRepository;
import com.ebanking.ebanking.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserService {

    UserRepository userRepository;

    @Autowired
    BankAccountRepository bankAccountRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository)
    {
        this.userRepository = userRepository;
    }

    /**
     * Gibt alle User zurück
     * @return liste aller User
     */
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    /**
     * Fügt einen User hinzu
     * @param user der User der hinzugefügt werden soll und gibt ihm gleich einen BankAccount
     * @return fertig gespeicherter User
     * @throws DuplicateUserNameException Wird geworfen wenn der User schon existiert
     */
    public User addUser(User user) throws DuplicateUserNameException {
        if (!userRepository.findByUsername(user.getUsername()).isPresent()) {

            user.setPassword(this.passwordEncoder.encode(user.getPassword()));

            BankAccount bankAccountnew = new BankAccount();

            //Iban generieren
            int max = 9;
            int min = 0;
            int range = max - min;
            String iban = "";
            for (int i = 0; i<11; i++){
                iban = iban + (int)+ (Math.random() * range);
            }

            //Bank Account erstellen
            bankAccountnew.setIban("AT8434000" + iban);
            bankAccountnew.setBic("RZBAATWW");
            bankAccountnew.setBalance(1000);
            bankAccountRepository.save(bankAccountnew);

            //Set erstellen zum hinzufügen des BankAccounts zum User
            user.setAccount(bankAccountnew);

            //User den Account zuweisen und Speichern des Users
            userRepository.save(user);

            return user;
        } else {
            throw new DuplicateUserNameException();
        }
    }

    /**
     * Gibt den User mit der id zurück
     * @param id des gesuchten Users
     * @return gefundener User
     * @throws UserNotFoundException Wird geworfen wenn der User nicht gefunden wurde
     */
    public User getUserById(Long id) throws UserNotFoundException {
        Optional<User> opu = userRepository.findById(id);
        if (opu.isPresent()) {
            return opu.get();
        } else {
            throw new UserNotFoundException();
        }
    }

    /**
     * Gibt den User mit der id zurück
     * @param name des gesuchten Users
     * @return gefundener User
     * @throws UserNotFoundException Wird geworfen wenn der User nicht gefunden wurde
     */
    public User getUserbyUsername(String name) throws UserNotFoundException {
        Optional<User> opu = userRepository.findByUsername(name);
        if (opu.isPresent()) {
            return opu.get();
        } else {
            throw new UserNotFoundException();
        }
    }

    /**
     * Löscht einen User mit der id
     * @param id des zu löschenden Users
     * @return gelöschter User
     */
    public User delete(long id){
        User user = null;
        try {
            user = getUserById(id);
        } catch (UserNotFoundException e) {
            e.printStackTrace();
        }
        if(user !=null)
        {
            userRepository.delete(user);
        }
        return user;
    }
    public void update (User user){userRepository.save(user);}

}

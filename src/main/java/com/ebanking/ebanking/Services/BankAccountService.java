package com.ebanking.ebanking.Services;

import com.ebanking.ebanking.Exceptions.DuplicateBankAccountException;
import com.ebanking.ebanking.Exceptions.UserNotFoundException;
import com.ebanking.ebanking.Models.BankAccount;
import com.ebanking.ebanking.Models.Transaction;
import com.ebanking.ebanking.Models.User;
import com.ebanking.ebanking.Repositories.BankAccountRepository;
import com.ebanking.ebanking.Repositories.TransactionRepository;
import com.ebanking.ebanking.Repositories.UserRepository;
import javafx.collections.ObservableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.security.auth.login.AccountNotFoundException;
import java.util.List;
import java.util.Optional;


@Service
public class BankAccountService {

    BankAccountRepository bankAccountRepository;
    UserRepository userRepository;

    public BankAccountService(BankAccountRepository bankAccountRepository, UserRepository userRepository)
    {
        this.bankAccountRepository = bankAccountRepository;
        this.userRepository = userRepository;
    }

    /**
     * Gibt alle Bank Accounts aus
     * @return Liste aller BankAccounts
     */
    public List<BankAccount> getAllAccounts() {
        return bankAccountRepository.findAll();
    }

    /**
     * Fügt einen BankAccount hinzu
     * @param bankAccount der BankAccount der hinzugefügt werden soll
     * @return fertiger BankAccounts
     * @throws DuplicateBankAccountException wird bei doppelten bank Accounts geworfen
     */
    public BankAccount addAccount(BankAccount bankAccount) throws DuplicateBankAccountException {
        return bankAccountRepository.save(bankAccount);
    }

    /**
     * Gibt BankAccount nach einer id
     * @param id des Bank Accounts
     * @return BankAccount
     * @throws AccountNotFoundException Wird geworfen wenn Account nicht geworfen wird
     */
    public BankAccount getAccountById(Long id) throws AccountNotFoundException {
        Optional<BankAccount> opa = bankAccountRepository.findById(id);
        if (opa.isPresent()) {
            return opa.get();
        } else {
            throw new AccountNotFoundException();
        }
    }

    /**
     * Löschen eines bestimmten BankAccounts
     * @param id des BankAccounts
     * @return gelöschter BankAccount
     */
    public BankAccount delete(long id){
        BankAccount bankAccount = null;
        try {
            bankAccount = getAccountById(id);
        } catch (AccountNotFoundException e) {
            e.printStackTrace();
        }
        if(bankAccount !=null)
        {
            bankAccountRepository.delete(bankAccount);
        }
        return bankAccount;
    }

    /**
     * Updatefunktion für einen BankAccount
     * @param bankAccount der zu updatende BankAccount
     * @return fertiger BankAccount
     */
    public BankAccount update (BankAccount bankAccount){bankAccountRepository.save(bankAccount);
        return bankAccount;
    }

    /**
     * Gibt die Accounts eines Users zurück
     * @param id id des Users
     * @return Liste der Accounts
     * @throws AccountNotFoundException
     * @throws UserNotFoundException
     */
    public BankAccount getAccountByUserId(Long id) throws AccountNotFoundException, UserNotFoundException {

        User user = userRepository.findByUserid(id);

        BankAccount account =  bankAccountRepository.findByUser(user);

        if (account != null) {
            return account;
        } else {
            throw new AccountNotFoundException();
        }
    }
}

package com.ebanking.ebanking.Services;

import com.ebanking.ebanking.Exceptions.BankAccountNotFound;
import com.ebanking.ebanking.Exceptions.TransactionErrorException;
import com.ebanking.ebanking.Exceptions.TransactionNotFoundException;
import com.ebanking.ebanking.Models.BankAccount;
import com.ebanking.ebanking.Models.Transaction;
import com.ebanking.ebanking.Repositories.BankAccountRepository;
import com.ebanking.ebanking.Repositories.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class TransactionService {
    TransactionRepository transactionRepository;

    @Autowired
    BankAccountRepository bankAccountRepository;

    public TransactionService(TransactionRepository transactionRepository)
    {
        this.transactionRepository = transactionRepository;
    }

    /**
     * Alle Transaktionen ausgeben
     * @return alle Transactions
     */
    public List<Transaction> getAllTransactions() {
        return transactionRepository.findAll();
    }

    /**
     * Transaktion erstellen
     * @param transaction zu erstellende Transaktion
     * @return fertige Transaction
     * @throws TransactionErrorException Wird geworfen bei Transaktionsfehlern
     */
    public Transaction addTransaction(Transaction transaction, long senderid, long recieverid ) throws TransactionErrorException, BankAccountNotFound {

            Optional<BankAccount> reciever = bankAccountRepository.findById(recieverid);
            Optional<BankAccount> sender = bankAccountRepository.findById(senderid);

            if(reciever.isPresent() && sender.isPresent()){

                BankAccount recieveraccount = reciever.get();
                BankAccount senderaccount = sender.get();

                transaction.setReceiver(recieveraccount);

                //Find by Iban
                //long senderid = transaction.getSender().getBankAccountid();
                //long empfaengerid = transaction.getReceiver().getBankAccountid();

                 //Datum auf Now setzen
                transaction.setDate(LocalDate.now());

                //Accounts finden per Id
                //TODO: ändern zu Transaktion mit Iban anlegen
                transaction.setSender(senderaccount);
                transaction.setReceiver(recieveraccount);

                //Werte der Konten Setzen. Falls ein Fehler auftritt Transaktion abbrechen
                if(transaction.getValue() > 0 && senderaccount.getBalance() > transaction.getValue()) {
                    senderaccount.setBalance(senderaccount.getBalance() - transaction.getValue());
                    recieveraccount.setBalance(recieveraccount.getBalance() + transaction.getValue());
                }else{
                    throw new TransactionErrorException();
                }
                return transactionRepository.save(transaction);
            } else
            {
                throw new BankAccountNotFound();
            }
    }

    /**
     * gibt eine Transaction mit der id zurück
     * @param id der Transaction
     * @return gefundene Transaction
     * @throws TransactionNotFoundException
     */
    public Transaction getTransactionById(Long id) throws TransactionNotFoundException {
        Optional<Transaction> opt = transactionRepository.findById(id);
        if (opt.isPresent()) {
            return opt.get();
        } else {
            throw new TransactionNotFoundException();
        }
    }

    /**
     * Gibt eine Transaktion nach BankAccount und zwischen daten zurück
     * @param id des BankAccounts
     * @param date1 Anfangsdatum
     * @param date2 Enddatum
     * @return liste der gefundenen Transaktionen
     * @throws TransactionNotFoundException
     */
    public List<Transaction> getTransactionByDate(long id, String date1, String date2) throws TransactionNotFoundException {

        //long lid = Long.parse(id);
        //BankAccount finden
        BankAccount bankAccount1 = bankAccountRepository.findByBankAccountid((long)id);
        BankAccount bankAccount2 = bankAccount1;

        //datum umwandeln
        LocalDate datelocal1 = LocalDate.parse(date1);
        LocalDate datelocal2 = LocalDate.parse(date2);

        //Alle Transactions des BankAccounts zwischen datum1 und datum2
        List<Transaction> transactions = transactionRepository.findAllBySenderOrReceiverAndDateIsBetween(bankAccount1, bankAccount2, datelocal1, datelocal2);

        if (transactions.size() > 0) {
            return transactions;
        } else {
            throw new TransactionNotFoundException();
        }
    }

    /**
     * gibt Transaktionen die zwischen der value1 und value2 sind als liste zurück
     * @param id id des BankAccounts
     * @param value1 Anfangswert
     * @param value2 Endwert
     * @return liste der Transaktionen
     * @throws TransactionNotFoundException
     */
    public List<Transaction> getTransactionsByValue(long id, String value1, String value2) throws TransactionNotFoundException {
        //BankAccount finden
        BankAccount bankAccount1 = bankAccountRepository.findByBankAccountid(id);
        BankAccount bankAccount2 = bankAccount1;

        //parse to double
        double doublev1 = Double.parseDouble(value1);
        double doublev2 = Double.parseDouble(value2);

        List<Transaction> transactions = transactionRepository.findAllBySenderOrReceiverAndValueBetween(bankAccount1, bankAccount2, doublev1, doublev2);

        if (transactions.size() > 0) {
            return transactions;
        } else {
            throw new TransactionNotFoundException();
        }
    }

    /**
     * Gibt eine Liste aller Transaktionen des BankAccounts wo ein Text im verwendungszweck oder in der referenz vorhanden ist
     * @param id des BankAccounts
     * @param verwendungszweck zu suchender Text
     * @param referenz zu suchender Text
     * @return liste aller zutreffenden Transaktionen
     * @throws TransactionNotFoundException
     */
    public List<Transaction> getTransactionsByText(long id, String verwendungszweck, String referenz) throws TransactionNotFoundException {
        //BankAccount finden
        BankAccount bankAccount1 = bankAccountRepository.findByBankAccountid(id);
        BankAccount bankAccount2 = bankAccount1;

        List<Transaction> transactions = transactionRepository.findAllBySenderOrReceiverAndVerwendungszweckContainsOrZahlungsreferenzContains(bankAccount1, bankAccount2, verwendungszweck, referenz);

        if (transactions.size() > 0) {
            return transactions;
        } else {
            throw new TransactionNotFoundException();
        }
    }


    public List<Transaction> getTransactionByAccountIdsender(long id) throws TransactionNotFoundException {
        BankAccount bankAccount1 = bankAccountRepository.findByBankAccountid(id);

        List<Transaction> transactions = transactionRepository.findAllBySender(bankAccount1);

        if (transactions.size() > 0) {
            return transactions;
        } else {
            throw new TransactionNotFoundException();
        }
    }

    public List<Transaction>  getTransactionByAccountIdempf(long id) throws TransactionNotFoundException {
        BankAccount bankAccount1 = bankAccountRepository.findByBankAccountid(id);

        List<Transaction> transactions = transactionRepository.findAllByReceiver(bankAccount1);

        if (transactions.size() > 0) {
            return transactions;
        } else {
            throw new TransactionNotFoundException();
        }
    }



    /**
     * Löschen einer Transaktionen
     * @param id der Transaktion
     * @return gelöschte Transaktion
     */
    public Transaction delete(long id){
        Transaction transaction = null;
        try {
            transaction = getTransactionById(id);
        } catch (TransactionNotFoundException e) {
            e.printStackTrace();
        }
        if(transaction !=null)
        {
            transactionRepository.delete(transaction);
        }
        return transaction;
    }
    public void update (Transaction transaction){transactionRepository.save(transaction);}



}

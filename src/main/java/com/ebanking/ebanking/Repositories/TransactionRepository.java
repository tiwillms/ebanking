package com.ebanking.ebanking.Repositories;

import com.ebanking.ebanking.Models.BankAccount;
import com.ebanking.ebanking.Models.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface TransactionRepository extends JpaRepository<Transaction, Long>  {

    /**
     * Transaktionen an diesem Datum
     * @param bankAccount BankAccount des Users
     * @param date Nach Datum finden
     * @return Transaction
     */
    Optional<Transaction> findBySenderAndDate(BankAccount bankAccount, LocalDate date);

    /**
     * Alle ausgehenden Transaktionen
     * @param bankAccount BankAccount des Users
     * @return Transaction
     */
    List<Transaction> findAllBySender(BankAccount bankAccount);

    /**
     * Alle eingehenden Transaktionen
     * @param bankAccount BankAccount des Users
     * @return Transaction
     */
    List<Transaction> findAllByReceiver(BankAccount bankAccount);

    /**
     * Transaktion nach Datum
     * @param bankAccount1 BankAccount des Users
     * @param bankAccount2 BankAccount des Users
     * @param date1 Startdatum
     * @param date2 Enddatum
     * @return Liste der Transactionen
     */
    List<Transaction> findAllBySenderOrReceiverAndDateIsBetween(BankAccount bankAccount1, BankAccount bankAccount2, LocalDate date1, LocalDate date2);

    /**
     * Transaktionen zwischen zwei Werten
     * @param bankAccount1 BankAccount des Users
     * @param bankAccount2 BankAccount des Users
     * @param value1 Wert1
     * @param value2 Wert2
     * @return Transactions
     */
    List<Transaction> findAllBySenderOrReceiverAndValueBetween(BankAccount bankAccount1, BankAccount bankAccount2, double value1, double value2);

    /**
     * Transaktionen finden wo Verwendungszweck oder Zahlungsreferenz den Text enthält
     * @param bankAccount1 BankAccount des Users
     * @param bankAccount2 BankAccount des Users
     * @param string1 String1
     * @param string2 String2
     * @return Transactions
     */
    List<Transaction> findAllBySenderOrReceiverAndVerwendungszweckContainsOrZahlungsreferenzContains (BankAccount bankAccount1, BankAccount bankAccount2, String string1, String string2);
}

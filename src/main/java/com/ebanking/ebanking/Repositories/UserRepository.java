package com.ebanking.ebanking.Repositories;

import com.ebanking.ebanking.Models.BankAccount;
import com.ebanking.ebanking.Models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Gibt den User zurück
     * @param username name des Users
     * @return User
     */
    Optional<User> findByUsername(String username);

    /**
     * Gibt den User mit der id zurück
     * @param id id des Users
     * @return User
     */
    User findByUserid(Long id);
}

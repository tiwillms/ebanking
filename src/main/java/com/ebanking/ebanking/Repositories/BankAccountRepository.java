package com.ebanking.ebanking.Repositories;

import com.ebanking.ebanking.Models.BankAccount;
import com.ebanking.ebanking.Models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BankAccountRepository extends JpaRepository<BankAccount, Long>  {

    /**
     * Bank Acount über die ID finden
     * @param id id des BankAccounts
     * @return BankAccount
     */
    BankAccount findByBankAccountid(Long id);

    /**
     * Bank Acount über die iban finden
     * @param iban iban des BankAccounts
     * @return BankAccount
     */
    BankAccount findByIban(String iban);

    /**
     * Alle BankAccounts des Users
     * @param user die ID des Users
     * @return Liste der BankAccounts
     */
    List<BankAccount> findAllByUser(User user);

    /**
     * Alle BankAccounts des Users
     * @param user die ID des Users
     * @return Liste der BankAccounts
     */
    BankAccount findByUser(User user);
}

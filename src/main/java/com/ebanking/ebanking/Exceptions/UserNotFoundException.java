package com.ebanking.ebanking.Exceptions;

public class UserNotFoundException extends Exception {
    public UserNotFoundException()
    {
        super("User nicht gefunden");
    }

}

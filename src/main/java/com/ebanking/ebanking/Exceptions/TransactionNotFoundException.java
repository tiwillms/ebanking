package com.ebanking.ebanking.Exceptions;

public class TransactionNotFoundException extends Exception {
    public TransactionNotFoundException()
    {
        super("Transaction nicht gefunden");
    }

}

package com.ebanking.ebanking.Exceptions;

public class BankAccountNotFound extends Exception {
    public BankAccountNotFound()
    {
        super("Bank Account not Found");
    }

}

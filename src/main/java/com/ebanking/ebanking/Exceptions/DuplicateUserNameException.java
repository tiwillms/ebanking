package com.ebanking.ebanking.Exceptions;

public class DuplicateUserNameException extends Exception {
    public DuplicateUserNameException()
    {
        super("User bereits vergeben");
    }

}

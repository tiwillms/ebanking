package com.ebanking.ebanking.Exceptions;

public class DuplicateRoleException extends Exception {
    public DuplicateRoleException()
    {
        super("Rolle bereits vergeben");
    }

}

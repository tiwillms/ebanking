package com.ebanking.ebanking.Exceptions;

public class DuplicateBankAccountException extends Exception {
    public DuplicateBankAccountException()
    {
        super("BankAccount bereits vergeben");
    }

}

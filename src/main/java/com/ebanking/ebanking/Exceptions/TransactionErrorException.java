package com.ebanking.ebanking.Exceptions;

public class TransactionErrorException extends Exception {
    public TransactionErrorException()
    {
        super("Transaction fehlgeschlagen");
    }

}

package com.ebanking.ebanking.Exceptions;

public class BankAccountFalseException extends Exception {
    public BankAccountFalseException()
    {
        super("Transaction nicht gefunden");
    }

}

package com.ebanking.ebanking.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class BankAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="bankAccountid")
    private Long bankAccountid;

    private String bic;

    @Column(unique = true)
    private String iban;

    private double balance;

    @OneToOne(mappedBy = "account")
    @JsonIgnore
    private User user;

    @OneToMany(mappedBy = "sender",cascade = CascadeType.PERSIST,fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Transaction> senderTransactions = new HashSet<>();

    @OneToMany(mappedBy = "receiver",cascade = CascadeType.PERSIST,fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Transaction> receiverTransactions = new HashSet<>();
}


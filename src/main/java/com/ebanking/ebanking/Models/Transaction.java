package com.ebanking.ebanking.Models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long transaction_id;

    @NotNull
    private double value;

    @NotNull
    private String zahlungsreferenz;

    private String verwendungszweck;

    @NotNull
    private LocalDate date;

    @ManyToOne
    private BankAccount sender;

    @ManyToOne
    private BankAccount receiver;
}


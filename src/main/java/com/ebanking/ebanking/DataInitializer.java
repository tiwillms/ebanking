package com.ebanking.ebanking;

import com.ebanking.ebanking.Models.BankAccount;
import com.ebanking.ebanking.Models.Transaction;
import com.ebanking.ebanking.Models.User;
import com.ebanking.ebanking.Repositories.BankAccountRepository;
import com.ebanking.ebanking.Repositories.TransactionRepository;
import com.ebanking.ebanking.Repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;

@Component
@Slf4j
public class DataInitializer implements CommandLineRunner {

    @Autowired
    UserRepository users;

    @Autowired
    BankAccountRepository bankAccountRepository;

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {

        BankAccount bankAccount1 = new BankAccount();
        bankAccount1.setBalance(1000000);
        bankAccount1.setBic("BIC1");
        bankAccount1.setIban("IBAN1");
        bankAccountRepository.save(bankAccount1);

        BankAccount bankAccount2 = new BankAccount();
        bankAccount2.setBalance(1000);
        bankAccount2.setBic("BIC2");
        bankAccount2.setIban("IBAN2");
        bankAccountRepository.save(bankAccount2);

        BankAccount bankAccount3 = new BankAccount();
        bankAccount3.setBalance(1000);
        bankAccount3.setBic("BIC3");
        bankAccount3.setIban("IBAN3");
        bankAccountRepository.save(bankAccount3);


        User u1 = new User();
        u1.setEmail("Bank@bank.com");
        u1.setPassword(this.passwordEncoder.encode("password"));
        u1.setUsername("Bank");
        u1.setRoles(Collections.singletonList("ADMIN"));

        users.save(u1);
        bankAccount1.setUser(u1);
        u1.setAccount(bankAccount1);

        User u2 = new User();
        u2.setEmail("thomas@test.com");
        u2.setPassword(this.passwordEncoder.encode("password"));
        u2.setUsername("thomas");
        u2.setRoles(Collections.singletonList("ROLE_USER"));

        users.save(u2);
        bankAccount2.setUser(u2);
        u2.setAccount(bankAccount2);

        User u3 = new User();
        u3.setEmail("timo@test.com");
        u3.setPassword(this.passwordEncoder.encode("password"));
        u3.setUsername("timo");
        u3.setRoles(Collections.singletonList("ROLE_USER"));

        users.save(u3);
        bankAccount2.setUser(u3);
        u3.setAccount(bankAccount3);

        users.save(u1);
        users.save(u2);
        users.save(u3);
        bankAccountRepository.save(bankAccount1);
        bankAccountRepository.save(bankAccount2);
        bankAccountRepository.save(bankAccount3);

        Transaction t = new Transaction();
        t.setDate(LocalDate.now());
        t.setSender(bankAccount1);
        t.setReceiver(bankAccount2);
        t.setValue(100);
        t.setVerwendungszweck("Testvw");
        t.setZahlungsreferenz("Testzr");
        transactionRepository.save(t);

        Transaction t2 = new Transaction();
        t2.setDate(LocalDate.now());
        t2.setSender(bankAccount2);
        t2.setReceiver(bankAccount1);
        t2.setValue(100);
        t2.setVerwendungszweck("Testvw2");
        t2.setZahlungsreferenz("Testzr2");
        transactionRepository.save(t2);

        Transaction t3 = new Transaction();
        t3.setDate(LocalDate.now());
        t3.setSender(bankAccount1);
        t3.setReceiver(bankAccount2);
        t3.setValue(50);
        t3.setVerwendungszweck("Testvw3");
        t3.setZahlungsreferenz("Testzr3");
        transactionRepository.save(t3);
        //bankAccount1.setBalance();

    }
}

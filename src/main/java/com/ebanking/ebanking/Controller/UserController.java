package com.ebanking.ebanking.Controller;

import com.ebanking.ebanking.Exceptions.DuplicateUserNameException;
import com.ebanking.ebanking.Exceptions.UserNotFoundException;
import com.ebanking.ebanking.Models.User;
import com.ebanking.ebanking.Repositories.UserRepository;
import com.ebanking.ebanking.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    UserRepository userRepository;
    UserService userService;

    public UserController(UserRepository userRepository, UserService userService) {
        this.userRepository = userRepository;
        this.userService = userService;
    }

    /**
     * Gibt alle User aus
     * @return liste aller User
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @GetMapping("/users")
    public ResponseEntity<List<User>> getAllUser() {
        return ResponseEntity.ok(userService.getAllUsers());
    }

    /**
     * Legt einen neuen User an und gibt ihm einen BankAccount
     * @param user neuer User
     * @return fertigen User
     * @throws DuplicateUserNameException wird geworfen wenn der Username bereits vorhanden ist
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @PostMapping("/user")
    public ResponseEntity<User> addUser(@RequestBody User user) throws DuplicateUserNameException {
        return ResponseEntity.ok(userService.addUser(user));
    }

    /**
     * Gibt einen entsprechenden User zurück
     * @param id id des Users
     * @return User
     * @throws UserNotFoundException wird geworfen wenn der User nicht gefunden wurde
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @GetMapping("/user/{id}")
    public ResponseEntity<User> getUserById(@PathVariable Long id) throws UserNotFoundException {
        return ResponseEntity.ok(this.userService.getUserById(id));
    }

    /**
     * Entgegengenommen wird ein Benutzername über Get
     * Gibt einen entsprechenden User zurück
     * @param name name des Users
     * @return User
     * @throws UserNotFoundException wird geworfen wenn der User nicht gefunden wurde
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @GetMapping("/user/name/{name}")
    public ResponseEntity<User> getUserByUsername(@PathVariable String name) throws UserNotFoundException {
        return ResponseEntity.ok(this.userService.getUserbyUsername(name));
    }

    /**
     * Update auf einen User
     * @param user der zu Updatende user
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @PutMapping("/user/{id}/edit")
    public void update(@RequestBody User user)
    {
         userService.update(user);
    }

    /**
     * löschen eines User
     * @param id des zu löschenden Users
     * @return der gelöschte User
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @DeleteMapping(path={"/user/{id}"})
    public User delete (@PathVariable("id") int id)
    {
        return userService.delete(id);
    }

}

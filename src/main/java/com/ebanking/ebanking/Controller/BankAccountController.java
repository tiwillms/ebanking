package com.ebanking.ebanking.Controller;

import com.ebanking.ebanking.Exceptions.DuplicateBankAccountException;
import com.ebanking.ebanking.Exceptions.UserNotFoundException;
import com.ebanking.ebanking.Models.BankAccount;
import com.ebanking.ebanking.Models.Transaction;
import com.ebanking.ebanking.Repositories.BankAccountRepository;
import com.ebanking.ebanking.Services.BankAccountService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.security.auth.login.AccountNotFoundException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class BankAccountController {

    BankAccountRepository bankAccountRepository;
    BankAccountService bankAccountService;

    public BankAccountController(BankAccountRepository bankAccountRepository, BankAccountService bankAccountService) {
        this.bankAccountRepository = bankAccountRepository;
        this.bankAccountService = bankAccountService;
    }

    /**
     * Gibt alle BankAccounts aus
     * @return liste aller BankAccounts
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @GetMapping("/accounts")
    public ResponseEntity<List<BankAccount>> getAllAccounts() {
        return ResponseEntity.ok(bankAccountService.getAllAccounts());
    }

    /**
     * Legt einen BankAccount an
     * @param bankAccount der zu anlegende BankAccount
     * @return gespeicherter BankAccount
     * @throws DuplicateBankAccountException wird geworfen wenn der BankAccount bereits vorhanden ist
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @PostMapping("/accounts")
    public ResponseEntity<BankAccount> addAccount(@RequestBody BankAccount bankAccount) throws DuplicateBankAccountException {
        return ResponseEntity.ok(bankAccountService.addAccount(bankAccount));
    }

    /**
     * gibt einen bestimmten BankAccount zurück
     * @param id die id des zu suchende BankAccount
     * @return der gesuchte BankAccount
     * @throws AccountNotFoundException wird geworfen wenn der Account nicht gefunden wurde
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @GetMapping("/account/{id}")
    public ResponseEntity<BankAccount> getAccountById(@PathVariable Long id) throws AccountNotFoundException {
        return ResponseEntity.ok(this.bankAccountService.getAccountById(id));
    }

    /**
     * Gibt alle BankAccounts eines Users aus
     * @param id des Users
     * @return liste aller BankAccounts des Users
     * @throws AccountNotFoundException wird geworfen wenn der BankAccount nicht gefunden wurde
     * @throws UserNotFoundException wird geworfen wenn der User nicht gefunden wurde
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @GetMapping("/accounts/user/{id}")
    public ResponseEntity<BankAccount> getAccountByUserId(@PathVariable Long id) throws AccountNotFoundException, UserNotFoundException {
        return ResponseEntity.ok(this.bankAccountService.getAccountByUserId(id));
    }

    /**
     * Update eines BankAccounts
     * @param bankAccount der zu updatende BankAccount
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @PutMapping("/account/{id}/edit")
    public void update(@RequestBody BankAccount bankAccount)
    {
         bankAccountService.update(bankAccount);
    }

    /**
     * Löschen eines BankAccounts
     * @param id id des zu löschenden BankAccounts
     * @return gelsöchter BankAccount
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @DeleteMapping(path={"/account/{id}"})
    public BankAccount delete (@PathVariable("id") int id)
    {
        return bankAccountService.delete(id);
    }

}

package com.ebanking.ebanking.Controller;

import com.ebanking.ebanking.Exceptions.BankAccountNotFound;
import com.ebanking.ebanking.Exceptions.TransactionErrorException;
import com.ebanking.ebanking.Exceptions.TransactionNotFoundException;
import com.ebanking.ebanking.Models.Transaction;
import com.ebanking.ebanking.Repositories.TransactionRepository;
import com.ebanking.ebanking.Services.TransactionService;
import org.apache.tomcat.jni.Local;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class TransactionController {

    TransactionRepository transactionRepository;
    TransactionService transactionService;

    public TransactionController(TransactionRepository transactionRepository, TransactionService transactionService) {
        this.transactionRepository = transactionRepository;
        this.transactionService = transactionService;
    }

    /**
     * Gibt alle Transactions zurück
     * @return Liste der Transactions
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @GetMapping("/transactions")
    public ResponseEntity<List<Transaction>> getAllTransactions() {
        return ResponseEntity.ok(transactionService.getAllTransactions());
    }

    /**
     * Legt eine Transaction an
     * @param transaction die Transaction
     * @return fertige Transaction
     * @throws TransactionErrorException Wird geworfen wenn ein Fehler in der Transaktion ist
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @PostMapping("/transaction/{sender}/{reciever}")
    public ResponseEntity<Transaction> addTransaction(@PathVariable long sender, @PathVariable long reciever, @RequestBody Transaction transaction) throws TransactionErrorException, BankAccountNotFound {
        return ResponseEntity.ok(transactionService.addTransaction(transaction, sender, reciever));
    }

    /**
     * Gibt eine bestimmte Transaktion aus (id)
     * @param id id der bestimmten Transaktion
     * @return bestimmte Transaktion
     * @throws TransactionNotFoundException wird bei Fehler geworfen
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @GetMapping("/transaction/{id}")
    public ResponseEntity<Transaction> getTransactionById(@PathVariable Long id) throws TransactionNotFoundException {
        return ResponseEntity.ok(this.transactionService.getTransactionById(id));
    }

    /**
     * Sucht alle gesendeten Transaktionen eines BankAccounts
     * @param id die id des BankAccounts
     * @return Liste der BankAccounts
     * @throws TransactionNotFoundException wird bei fehler geworfen
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @GetMapping("/transaction/{id}/sender")
    public ResponseEntity<List<Transaction>> transactionsfromaccountsender(@PathVariable long id) throws TransactionNotFoundException {
        return ResponseEntity.ok(transactionService.getTransactionByAccountIdsender(id));
    }

    /**
     * Sucht alle empfangenen Transaktionen eines BankAccounts
     * @param id die id des BankAccounts
     * @return Liste der BankAccounts
     * @throws TransactionNotFoundException wird bei fehler geworfen
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @GetMapping("/transaction/{id}/empf")
    public ResponseEntity<List<Transaction>> transactionsfromaccountempf(@PathVariable long id) throws TransactionNotFoundException {
        return ResponseEntity.ok(transactionService.getTransactionByAccountIdempf(id));
    }

    /**
     * Sucht alle Transaktionen eines BankAccounts zwischen datum1 und datum2
     * @param datum1 Anfangsdatum
     * @param datum2 Enddatum
     * @param id die id des BankAccounts
     * @return Liste der BankAccounts
     * @throws TransactionNotFoundException wird bei fehler geworfen
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @GetMapping("/transaction/date/{id}/{datum1}/{datum2}")
    public ResponseEntity<List<Transaction>> transactionsbetweendate(@PathVariable long id, @PathVariable String datum1, @PathVariable String datum2) throws TransactionNotFoundException {
        return ResponseEntity.ok(transactionService.getTransactionByDate(id, datum1, datum2));
    }

    /**
     * Suche alle Transaktionen eines BankAccounts zwischen value1 und value2
     * @param value1 Anfangswert
     * @param value2 Endwert
     * @param id id des BankAccounts
     * @return Liste der Transaktionen
     * @throws TransactionNotFoundException wird bei fehler geworfen
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @GetMapping("/transaction/value/{id}/{value1}/{value2}")
    public ResponseEntity<List<Transaction>> transactionsbetweenvalue(@PathVariable long id,@PathVariable String value1,@PathVariable String value2) throws TransactionNotFoundException {
        return ResponseEntity.ok(transactionService.getTransactionsByValue(id, value1, value2));
    }

    /**
     * Suche alle Transaktionen eines BankAccounts wo im verwendungszweck oder in referenz ein bestimmter string vorhanden ist
     * @param verwend zu suchender String
     * @param ref zu suchender String
     * @param id id des BankAccounts
     * @return Liste der Transaktionen
     * @throws TransactionNotFoundException wird bei fehler geworfen
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @GetMapping("/transaction/text/{id}/{verwend}/{ref}")
    public ResponseEntity<List<Transaction>> transactionsBytext(@PathVariable long id,@PathVariable String verwend,@PathVariable String ref) throws TransactionNotFoundException {
        return ResponseEntity.ok(transactionService.getTransactionsByText(id, verwend, ref));
    }

    /**
     * Update auf eine Transaction
     * @param transaction die zu updatende Transaction
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @PutMapping("/transaction/{id}/edit")
    public void update(@RequestBody Transaction transaction)
    {
         transactionService.update(transaction);
    }

    /**
     * Löschen einer Transaction
     * @param id die id der zu Löschenden Transaction
     * @return gelöschte Transaction
     */
    @CrossOrigin (origins = "http://localhost:4200")
    @DeleteMapping(path={"/transaction/{id}"})
    public Transaction delete (@PathVariable("id") int id)
    {
        return transactionService.delete(id);
    }

}

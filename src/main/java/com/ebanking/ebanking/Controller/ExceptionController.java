package com.ebanking.ebanking.Controller;

import com.ebanking.ebanking.Exceptions.*;
import com.ebanking.ebanking.Token.InvalidJwtAuthenticationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice
@CrossOrigin(origins = "http://localhost:4200")
public class ExceptionController {

    @ExceptionHandler(BankAccountFalseException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<ExceptionResponseEntity> handleBankAccountFalse (BankAccountFalseException ex) {

        ExceptionResponseEntity exre = new ExceptionResponseEntity("1000", ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(BankAccountNotFound.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ExceptionResponseEntity> handleBankAccountNotFound (BankAccountNotFound ex) {

        ExceptionResponseEntity exre = new ExceptionResponseEntity("1001", ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(DuplicateBankAccountException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ExceptionResponseEntity> handleDuplicateBankAccount (DuplicateBankAccountException ex) {

        ExceptionResponseEntity exre = new ExceptionResponseEntity("1001", ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(DuplicateUserNameException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<ExceptionResponseEntity> handleDuplicateUserNameException (DuplicateUserNameException ex) {

        ExceptionResponseEntity exre = new ExceptionResponseEntity("1002", ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(DuplicateRoleException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<ExceptionResponseEntity> handleDuplicateRole (DuplicateRoleException ex) {

        ExceptionResponseEntity exre = new ExceptionResponseEntity("1002", ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(TransactionErrorException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<ExceptionResponseEntity> handleTransactionErrorException (TransactionErrorException ex) {

        ExceptionResponseEntity exre = new ExceptionResponseEntity("1004", ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(TransactionNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ExceptionResponseEntity> handleTransactionNotFoundException (TransactionNotFoundException ex) {

        ExceptionResponseEntity exre = new ExceptionResponseEntity("1005", ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ExceptionResponseEntity> handleUserNotFoundException (UserNotFoundException ex) {

        ExceptionResponseEntity exre = new ExceptionResponseEntity("1005", ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvalidJwtAuthenticationException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<ExceptionResponseEntity> handleInvalidJwtAuthenticationException (InvalidJwtAuthenticationException ex) {

        ExceptionResponseEntity exre = new ExceptionResponseEntity("1000", ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.CONFLICT);
    }

}
